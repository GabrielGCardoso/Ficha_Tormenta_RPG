
public class Per�cias extends Personagem {

	private String NomePericia;
	private int Grad;
	private boolean TreinadaNTreinada;

	public Per�cias() {

	}

	public void criarPericia(String paramNomePericia, int paramNivel, int pMod, boolean pTreinadaNTreinada) {
		NomePericia = paramNomePericia;
		if (pTreinadaNTreinada == true) { // � Treinada
			setGrad(paramNivel + 2 + pMod);
		} else
			setGrad((paramNivel / 2) + pMod); // � Treinada
	}

	public void setGrad(int paramGrad) {
		Grad = paramGrad;
	}
	
	public void setTreinadaNTreinada(boolean paramTreinadaNTreinada){
		TreinadaNTreinada = paramTreinadaNTreinada;
	}

	public void addOutros(int pOutros) {
		Grad = Grad + pOutros;
	}
}
