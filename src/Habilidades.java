
public class Habilidades {

	private int ModFOR;
	public int FOR;
	private int ModDES;
	public int DES;
	private int ModCON;
	public int CON;
	private int ModINT;
	public int INT;
	private int ModSAB;
	public int SAB;
	private int ModCAR;
	public int CAR;

	public Habilidades() {

	}

	public Habilidades(int pFOR, int pDES, int pCON, int pINT, int pSAB, int pCAR) {

		ModFOR = (pFOR - 10) / 2;
		if (pFOR < 10) {
			ModFOR = (pFOR - 11) / 2;
		}
		ModDES = (pDES - 10) / 2;
		if (pDES < 10) {
			ModDES = (pDES - 11) / 2;
		}
		ModCON = (pCON - 10) / 2;
		if (pCON < 10) {
			ModCON = (pCON - 11) / 2;
		}
		ModINT = (pINT - 10) / 2;
		if (pINT < 10) {
			ModINT = (pINT - 11) / 2;
		}
		ModSAB = (pSAB - 10) / 2;
		if (pSAB < 10) {
			ModFOR = (pSAB - 11) / 2;
		}
		ModCAR = (pCAR - 10) / 2;
		if (pCAR < 10) {
			ModCAR = (pCAR - 11) / 2;
		}
	}

	public int getModFOR() {
		return ModFOR;
	}

	public void setModFOR(int modFOR) {
		ModFOR = modFOR;
	}

	public int getFOR() {
		return FOR;
	}

	public void setFOR(int fOR) {
		FOR = fOR;
	}

	public int getModDES() {
		return ModDES;
	}

	public void setModDES(int modDES) {
		ModDES = modDES;
	}

	public int getDES() {
		return DES;
	}

	public void setDES(int dES) {
		DES = dES;
	}

	public int getModCON() {
		return ModCON;
	}

	public void setModCON(int modCON) {
		ModCON = modCON;
	}

	public int getCON() {
		return CON;
	}

	public void setCON(int cON) {
		CON = cON;
	}

	public int getModINT() {
		return ModINT;
	}

	public void setModINT(int modINT) {
		ModINT = modINT;
	}

	public int getINT() {
		return INT;
	}

	public void setINT(int iNT) {
		INT = iNT;
	}

	public int getModSAB() {
		return ModSAB;
	}

	public void setModSAB(int modSAB) {
		ModSAB = modSAB;
	}

	public int getSAB() {
		return SAB;
	}

	public void setSAB(int sAB) {
		SAB = sAB;
	}

	public int getModCAR() {
		return ModCAR;
	}

	public void setModCAR(int modCAR) {
		ModCAR = modCAR;
	}

	public int getCAR() {
		return CAR;
	}

	public void setCAR(int cAR) {
		CAR = cAR;
	}

}
