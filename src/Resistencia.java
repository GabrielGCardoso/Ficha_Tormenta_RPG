
public class Resistencia extends Habilidades {
	
	private int FORT;
	private int REF;
	private int VON;
	
	public Resistencia() {
	}
	
	public Resistencia(int pFOR, int pDES, int pCON, int pINT, int pSAB, int pCAR) {
		super(pFOR, pDES, pCON, pINT, pSAB, pCAR);
	}

	public int getFORT(int paramNivel) {
		FORT = (paramNivel / 2) + getModCON();
		return FORT;
	}

	public int getREF(int paramNivel) {
		REF = (paramNivel / 2) + getModDES();
		return REF;
	}

	public int getVON(int paramNivel) {
		VON = (paramNivel / 2) + getModSAB();
		return VON;
	}
}
